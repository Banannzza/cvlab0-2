//
//  GrayWorldFilter.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 12/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import AVFoundation
import Accelerate

class GrayWorldFilter: Filter {
    
    var bitmapInfo: CGBitmapInfo!
    
    init(bitmapInfo: CGBitmapInfo) {
        self.bitmapInfo = bitmapInfo
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func applayTo(bufferData: (width: Int, height: Int, bytesPerRow: Int, bufferData: CFMutableData)?) -> CGImage? {
        if let bufferInfo = bufferData {
            let int8Buffer = CFDataGetMutableBytePtr(bufferInfo.bufferData)!
            let numberOfPixels = bufferInfo.width * bufferInfo.height
            var blueVector:[Float] = Array(repeating: 0.0, count: numberOfPixels)
            var greenVector:[Float] = Array(repeating: 0.0, count: numberOfPixels)
            var redVector:[Float] = Array(repeating: 0.0, count: numberOfPixels)
            
            vDSP_vfltu8(int8Buffer, 4, &blueVector, 1, vDSP_Length(numberOfPixels))
            vDSP_vfltu8(int8Buffer + 1, 4, &greenVector, 1, vDSP_Length(numberOfPixels))
            vDSP_vfltu8(int8Buffer + 2, 4, &redVector, 1, vDSP_Length(numberOfPixels))
            
            var redAverage:Float = 0.0
            var blueAverage:Float = 0.0
            var greenAverage:Float = 0.0
            
            vDSP_meamgv(&redVector, 1, &redAverage, vDSP_Length(numberOfPixels))
            vDSP_meamgv(&greenVector, 1, &greenAverage, vDSP_Length(numberOfPixels))
            vDSP_meamgv(&blueVector, 1, &blueAverage, vDSP_Length(numberOfPixels))
            
            let avarage = (redAverage + blueAverage + greenAverage) / 3.0
            
            let blueComponent = blueAverage != 0.0 ? (avarage / blueAverage) : 0.0
            let greenComponent = greenAverage != 0.0 ? (avarage / greenAverage) : 0.0
            let redComponent = redAverage != 0.0 ? (avarage / redAverage) : 0.0
            
            var index = -4
            for i in 0..<numberOfPixels {
                index += 4
                int8Buffer[index] = UInt8((blueVector[i] * blueComponent).truncatingRemainder(dividingBy: 256))
                int8Buffer[index + 1] = UInt8((greenVector[i] * greenComponent).truncatingRemainder(dividingBy: 256))
                int8Buffer[index + 2] = UInt8((redVector[i] * redComponent).truncatingRemainder(dividingBy: 256))
            }
            
            let colorSpace = CGColorSpaceCreateDeviceRGB();
           
            //let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue).union(.byteOrder32Little)
            let context = CGContext(data: int8Buffer, width: bufferInfo.width, height: bufferInfo.height, bitsPerComponent: 8, bytesPerRow: bufferInfo.bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
            let image = context?.makeImage()
            return image;
        }
        return nil;
    }
}
