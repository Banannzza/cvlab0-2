//
//  HalfToneFilter.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 13/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import AVFoundation

class HalfToneFilter: Filter {

    var bitmapInfo: CGBitmapInfo!
    var blueRatio: Float!
    var greenRatio: Float!
    var redRatio: Float!
    
    init(bitmapInfo: CGBitmapInfo) {
        self.bitmapInfo = bitmapInfo
        self.redRatio = self.bitmapInfo == CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue).union(.byteOrder32Little) ? 0.3 : 0.11
        self.greenRatio = 0.59
        self.blueRatio = redRatio == 0.3 ? 0.11 : 0.3
    }


    func applayTo(bufferData: (width: Int, height: Int, bytesPerRow: Int, bufferData: CFMutableData)?) -> CGImage? {
        if let bufferInfo = bufferData {
            let int8Buffer = CFDataGetMutableBytePtr(bufferInfo.bufferData)!
            let numberOfPixels = bufferInfo.width * bufferInfo.height
            var index = -4
            for _ in 0..<numberOfPixels {
                index += 4;
                let pr = Float(int8Buffer[index]) * self.blueRatio! + (Float(int8Buffer[index + 1]) * self.greenRatio! + Float(int8Buffer[index + 2]) * self.redRatio!)
                
                let gray: UInt8 = UInt8(pr.truncatingRemainder(dividingBy: 256))
                int8Buffer[index] = gray
                int8Buffer[index + 1] = gray
                int8Buffer[index + 2] = gray
            }
            
            let colorSpace = CGColorSpaceCreateDeviceRGB();
           // let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue).union(.byteOrder32Little)
            let context = CGContext(data: int8Buffer, width: bufferInfo.width, height: bufferInfo.height, bitsPerComponent: 8, bytesPerRow: bufferInfo.bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
            let image = context?.makeImage()
            return image;
        }
        return nil;
    }
}
