//
//  Filter.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 12/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import AVFoundation
import Accelerate





protocol Filter {
    var bitmapInfo: CGBitmapInfo! {get set}
    func applyTo(sampleBuffer: CMSampleBuffer) -> CGImage?
    func applyTo(sampleImage: CGImage?) -> CGImage?
    func applayTo(bufferData: (width: Int, height: Int, bytesPerRow: Int, bufferData: CFMutableData)?) -> CGImage?
}

extension Filter {
  
    func getInfoFrom(sampleBuffer: CMSampleBuffer) -> (width: Int, height: Int, bytesPerRow: Int, bufferData: CFMutableData)? {
        let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        if let buffer = imageBuffer {
            CVPixelBufferLockBaseAddress(buffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
            let width = CVPixelBufferGetWidth(buffer)
            let height = CVPixelBufferGetHeight(buffer)
            let bytesPerRow = CVPixelBufferGetBytesPerRow(buffer)
            let pixels = CVPixelBufferGetBaseAddressOfPlane(buffer, 0)
            let pixelsPtr = unsafeBitCast(pixels, to: UnsafePointer<UInt8>.self)
            let copyData = CFDataCreateMutable(nil, 0)
            CFDataAppendBytes(copyData, pixelsPtr, 4 * width * height)
           
            CVPixelBufferUnlockBaseAddress(buffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
            return (width, height, bytesPerRow, copyData!)
        }
        return nil
    }
    func applyTo(sampleImage: CGImage?) -> CGImage? {
        if (sampleImage != nil) {
           // let numberOfPixels = sampleImage!.width * sampleImage!.height
            let rawData = sampleImage!.dataProvider!.data;
           
            let copyData = CFDataCreateMutable(nil, 0)
            let pixels = CFDataGetBytePtr(rawData as CFData!)!;
            CFDataAppendBytes(copyData, pixels, 4 * sampleImage!.width * sampleImage!.height)
            return self.applayTo(bufferData: (sampleImage!.width, sampleImage!.height, sampleImage!.bytesPerRow, copyData!))
        }
        return nil
    }
    
    func applyTo(sampleBuffer: CMSampleBuffer) -> CGImage? {
        if let bufferInfo = self.getInfoFrom(sampleBuffer: sampleBuffer) {
            
            return self.applayTo(bufferData: bufferInfo)
        }
        return nil
    }
}
