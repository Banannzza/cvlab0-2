//
//  GammaFilter.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 14/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import AVFoundation
import OpenGLES
import GLKit

class GammaFilter: Filter {

    
    let isVideo: Bool!
    var bitmapInfo: CGBitmapInfo!
    var yValue: Float!
    var cValue: Float!
    
    // Typealias for RGB color values
    typealias RGB = (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)
    
    // Typealias for HSV color values
    typealias HSV = (hue: CGFloat, saturation: CGFloat, brightness: CGFloat, alpha: CGFloat)
    
    
    init(bitmapInfo: CGBitmapInfo) {
        self.bitmapInfo = bitmapInfo
        self.isVideo = self.bitmapInfo == CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue).union(.byteOrder32Little)
        self.yValue = 1
        self.cValue = 1
    }
    
    func setParameters(y: Float, c: Float) {
        self.cValue = c
        self.yValue = y
    }
    
    func applayTo(bufferData: (width: Int, height: Int, bytesPerRow: Int, bufferData: CFMutableData)?) -> CGImage? {
            if let bufferInfo = bufferData {
                let int8Buffer = CFDataGetMutableBytePtr(bufferInfo.bufferData)!
                let numberOfPixels = bufferInfo.width * bufferInfo.height
                
                let localCValue = self.cValue!
                let localYValue = self.yValue!
                var r,g,b, a: CGFloat
                var hue : CGFloat = 0,
                sat: CGFloat = 0,
                bri: CGFloat = 0,
                alp: CGFloat = 0,
                newR: CGFloat = 0,
                newB: CGFloat = 0,
                newG: CGFloat = 0
                var index = -4
                if (localYValue != 1 || localCValue != 1) {
                    for _ in 0..<numberOfPixels{
                        index += 4
                        b = CGFloat(int8Buffer[index]) / 255.0
                        g = CGFloat(int8Buffer[index + 1]) / 255.0
                        r = CGFloat(int8Buffer[index + 2]) / 255.0
                        a = CGFloat(int8Buffer[index + 3]) / 255.0
                        let originalColor = UIColor(red: isVideo! ? r : b, green: g, blue: isVideo! ? b : r, alpha: a)
                        originalColor.getHue(&hue, saturation: &sat, brightness: &bri, alpha: &alp)
                        let newBrightness = CGFloat(cValue * pow(Float(bri), yValue))
                        let newColor = UIColor(hue: hue, saturation: sat, brightness: newBrightness, alpha: alp)
                        newColor.getRed(&newR, green: &newG, blue: &newB, alpha: &alp)
                        int8Buffer[isVideo! ? index : index + 2] = UInt8((newB * 255).truncatingRemainder(dividingBy: 256))
                        int8Buffer[index + 1] = UInt8((newG * 255).truncatingRemainder(dividingBy: 256))
                        int8Buffer[isVideo! ? index + 2 : index] = UInt8((newR * 255).truncatingRemainder(dividingBy: 256))
                        int8Buffer[index + 3] = UInt8((alp * 255).truncatingRemainder(dividingBy: 256))
                    }
                }
                
                let colorSpace = CGColorSpaceCreateDeviceRGB();
             //   let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue).union(.byteOrder32Big)
                let context = CGContext(data: int8Buffer, width: bufferInfo.width, height: bufferInfo.height, bitsPerComponent: 8, bytesPerRow: bufferInfo.bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
                let image = context?.makeImage()
                return image;
            }
            return nil
    }
}
