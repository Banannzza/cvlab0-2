//
//  ControlColorFilter.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 14/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import AVFoundation

struct Color {
    var B: UInt8
    var G: UInt8
    var R: UInt8
    var A: UInt8
    
    init(B: UInt8, G: UInt8, R: UInt8, A: UInt8 = 255) {
        self.B = B
        self.G = G
        self.R = R
        self.A = A
    }
    init(R: UInt8, G: UInt8, B: UInt8, A: UInt8 = 255) {
        self.B = B
        self.G = G
        self.R = R
        self.A = A
    }
}

protocol ControlColorFilterDelegate {
    func getPixelColor(color: Color)
}

class ControlColorFilter: Filter {
    
    var bitmapInfo: CGBitmapInfo!
    var controlCollor: Color?
    var sourceCollor: Color?
    var pixel: CGPoint?
    var scaleSize: CGSize?
    var delegate: ControlColorFilterDelegate?
    var mainAlgorythm: ((UnsafeMutablePointer<UInt8>, Int, Color, Color) -> Void)!

    init(bitmapInfo: CGBitmapInfo) {
        self.bitmapInfo = bitmapInfo
        if bitmapInfo == CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue).union(.byteOrder32Little) {
            self.mainAlgorythm = self.algorythmForBGRA
        }
        else {
            self.mainAlgorythm = self.algorythmForRGBA
        }
    }
    
    func setPixel(point: CGPoint, scaleSize: CGSize) {
        self.pixel = point
        self.scaleSize =  scaleSize
        self.controlCollor = nil
        self.sourceCollor = nil
    }
    
    func setControlCollor(color: Color) {
        self.controlCollor = color
    }
    
    func clearSelectedColors() {
        self.controlCollor = nil
        self.sourceCollor = nil
    }

    func applayTo(bufferData: (width: Int, height: Int, bytesPerRow: Int, bufferData: CFMutableData)?) -> CGImage? {
        if let bufferInfo = bufferData {
            let int8Buffer = CFDataGetMutableBytePtr(bufferInfo.bufferData)!
            let numberOfPixels = bufferInfo.width * bufferInfo.height
            
            
            if let dstColor = self.controlCollor {
                if let srcColor = self.sourceCollor {
                    self.mainAlgorythm(int8Buffer, numberOfPixels, srcColor, dstColor)
                }
            }
            
            
            if let pixel = self.pixel {
                let xScale = CGFloat(bufferInfo.width) / self.scaleSize!.width
                let yScale = CGFloat(bufferInfo.height) / self.scaleSize!.height
                let newX = Int(pixel.x * xScale)
                let newY = Int(pixel.y * yScale)
                
                let index = (newY * bufferInfo.width + newX) * 4
                var color: Color!
                if self.bitmapInfo == CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue).union(.byteOrder32Little) {
                    color = Color(B: int8Buffer[index], G: int8Buffer[index + 1], R: int8Buffer[index + 2], A: int8Buffer[index + 3])
                }
                else {
                    color = Color(R: int8Buffer[index], G: int8Buffer[index + 1], B: int8Buffer[index + 2], A: int8Buffer[index + 3])
                }
                self.sourceCollor = self.sourceCollor == nil ? color : self.sourceCollor
                self.delegate?.getPixelColor(color: color)
                self.pixel = nil
            }
            
            let colorSpace = CGColorSpaceCreateDeviceRGB();

            let context = CGContext(data: int8Buffer, width: bufferInfo.width, height: bufferInfo.height, bitsPerComponent: 8, bytesPerRow: bufferInfo.bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
            let image = context?.makeImage()
            return image;
            
        }
        return nil
    }
    
    
    func algorythmForBGRA(dataPointer: UnsafeMutablePointer<UInt8>, numberOfPixels: Int, srcColor: Color, dstColor: Color) {
        var index = -4
        var count = 0
        for _ in 0..<numberOfPixels {
            index += 4;
            //            if (self.controlCollor?.B != dataPointer[index] || self.controlCollor?.G != dataPointer[index + 1] || self.controlCollor?.A == dataPointer[index + 2] ) {
            //                continue;
            //            }
            let flag = Int32(srcColor.B) - Int32(dataPointer[index]) + Int32(srcColor.G) - Int32(dataPointer[index + 1]) + Int32(srcColor.R) - Int32(dataPointer[index + 2])
            let ch = abs(flag) == 0
//            if (ch) {
//                count += 1
//            }
//            let newB = srcColor.B == 0 ? 0 : CGFloat(dataPointer[index]) * CGFloat(dstColor.B) / CGFloat(srcColor.B)
//            let newG = srcColor.G == 0 ? 0 : CGFloat(dataPointer[index + 1]) * CGFloat(dstColor.G) / CGFloat(srcColor.G)
//            let newR = srcColor.R == 0 ? 0 : CGFloat(dataPointer[index + 2]) * CGFloat(dstColor.R) / CGFloat(srcColor.R)
            dataPointer[index] = ch ? UInt8((srcColor.B == 0 ? 0 : CGFloat(dataPointer[index]) * CGFloat(dstColor.B) / CGFloat(srcColor.B)).truncatingRemainder(dividingBy: 256)) : dataPointer[index]
            dataPointer[index + 1] = ch ? UInt8((srcColor.G == 0 ? 0 : CGFloat(dataPointer[index + 1]) * CGFloat(dstColor.G) / CGFloat(srcColor.G)).truncatingRemainder(dividingBy: 256)) : dataPointer[index + 1]
            dataPointer[index + 2] = ch ? UInt8((srcColor.R == 0 ? 0 : CGFloat(dataPointer[index + 2]) * CGFloat(dstColor.R) / CGFloat(srcColor.R)).truncatingRemainder(dividingBy: 256)) : dataPointer[index + 2]
        }
        print(count)
    }
    
    func algorythmForRGBA(dataPointer: UnsafeMutablePointer<UInt8>, numberOfPixels: Int, srcColor: Color, dstColor: Color) {
        var index = -4
        var count = 0
        for _ in 0..<numberOfPixels {
            index += 4;
            //            if (self.controlCollor?.B != dataPointer[index] || self.controlCollor?.G != dataPointer[index + 1] || self.controlCollor?.A == dataPointer[index + 2] ) {
            //                continue;
            //            }
            let flag = Int32(srcColor.R) - Int32(dataPointer[index]) + Int32(srcColor.G) - Int32(dataPointer[index + 1]) + Int32(srcColor.B) - Int32(dataPointer[index + 2])
            let ch = abs(flag) == 0
            //            if (ch) {
            //                count += 1
            //            }
            //            let newB = srcColor.B == 0 ? 0 : CGFloat(dataPointer[index]) * CGFloat(dstColor.B) / CGFloat(srcColor.B)
            //            let newG = srcColor.G == 0 ? 0 : CGFloat(dataPointer[index + 1]) * CGFloat(dstColor.G) / CGFloat(srcColor.G)
            //            let newR = srcColor.R == 0 ? 0 : CGFloat(dataPointer[index + 2]) * CGFloat(dstColor.R) / CGFloat(srcColor.R)
            dataPointer[index + 2] = ch ? UInt8((srcColor.B == 0 ? 0 : CGFloat(dataPointer[index + 2]) * CGFloat(dstColor.B) / CGFloat(srcColor.B)).truncatingRemainder(dividingBy: 256)) : dataPointer[index + 2]
            dataPointer[index + 1] = ch ? UInt8((srcColor.G == 0 ? 0 : CGFloat(dataPointer[index + 1]) * CGFloat(dstColor.G) / CGFloat(srcColor.G)).truncatingRemainder(dividingBy: 256)) : dataPointer[index + 1]
            dataPointer[index] = ch ? UInt8((srcColor.R == 0 ? 0 : CGFloat(dataPointer[index]) * CGFloat(dstColor.R) / CGFloat(srcColor.R)).truncatingRemainder(dividingBy: 256)) : dataPointer[index]
        }
        print(count)
    }
    
}
