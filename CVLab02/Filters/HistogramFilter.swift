//
//  HistogramFilter.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 14/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import AVFoundation
import Accelerate

class HistogramFilter: Filter {
    
    enum Channel{
        case Blue
        case Green
        case Red
    }
    
    var bitmapInfo: CGBitmapInfo!
    let isVideo: Bool!
    var histogramWidth: CGFloat!
    var histogramHeight: CGFloat!
    var channel:Channel!
    var histogramData: [Int]!
    var numberOfPixels: Int!
    
    init(bitmapInfo: CGBitmapInfo, channel: Channel, forWidth: CGFloat, forHeight: CGFloat) {
        self.bitmapInfo = bitmapInfo
        self.channel = channel
        self.histogramWidth = forWidth
        self.histogramHeight = forHeight
        self.isVideo = self.bitmapInfo == CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue).union(.byteOrder32Little)
    }
    
    func setSize(width: CGFloat, height: CGFloat) {
        self.histogramWidth = width
        self.histogramHeight = height

    }

    
    func makeBeziePath(histogram: [Int], numberOfPixels: Int) -> UIBezierPath {
        let bezier = UIBezierPath()
        let stepForX = self.histogramWidth / 255
        var max = histogram.max()!
        bezier.move(to: CGPoint(x: 0, y: self.histogramHeight))
        for channelValue in 0..<256 {
            let xPos = CGFloat(channelValue) * stepForX
            let yPos = self.histogramHeight - CGFloat(histogram[channelValue]) / CGFloat(max) * (self.histogramHeight)
            bezier.move(to: CGPoint(x: xPos, y: self.histogramHeight))
            bezier.addLine(to: CGPoint(x:xPos, y: yPos))
        }
       // bezier.addLine(to: CGPoint(x: CGFloat(self.histogramWidth) * stepForX, y: CGFloat(self.histogramHeight)))
       // bezier.addLine(to: CGPoint(x: 0 * stepForX, y: CGFloat(self.histogramHeight)))
        bezier.close()
        return bezier
    }
    
    func getBezier() -> UIBezierPath? {
        return makeBeziePath(histogram: self.histogramData!, numberOfPixels: self.numberOfPixels)
    }
    
    func convertPathToImage(path: UIBezierPath) -> CGImage?
    {
        let strokeColor:UIColor!
        switch self.channel! {
        case .Blue:
            strokeColor = UIColor.blue
            break
        case .Green:
            strokeColor = UIColor.green
            break
        case .Red:
            strokeColor = UIColor.red
            break
        }

        UIGraphicsBeginImageContextWithOptions(CGSize(width: self.histogramWidth, height: self.histogramHeight), false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        context!.setStrokeColor(strokeColor.cgColor)
        context!.setLineWidth(self.histogramWidth / 256)
     //   context!.setFillColor(strokeColor.cgColor)
        path.stroke()
      //  path.fill()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image?.cgImage
    }
    
    
    func applayTo(bufferData: (width: Int, height: Int, bytesPerRow: Int, bufferData: CFMutableData)?) -> CGImage? {
        if let bufferInfo = bufferData {
            let int8Buffer = CFDataGetMutableBytePtr(bufferInfo.bufferData)!
            self.numberOfPixels = bufferInfo.width * bufferInfo.height
            
            var channelVector:[Float] = Array(repeating: 0.0, count: self.numberOfPixels)

            
            vDSP_vfltu8(self.channel! == .Red ? self.isVideo! ? int8Buffer + 2 : int8Buffer : self.channel! == .Blue ? self.isVideo! ? int8Buffer : int8Buffer + 2 : int8Buffer + 1, 4, &channelVector, 1, vDSP_Length(self.numberOfPixels))
            
            
            var histogram = [Int](repeating:0, count: 256)
            for i in 0..<channelVector.count {
                let index = Int(channelVector[i])
                //print(index)
                histogram[index] += 1
            }
            
            self.histogramData = histogram
            let bezier = self.makeBeziePath(histogram: histogram, numberOfPixels: self.numberOfPixels)
            return self.convertPathToImage(path: bezier)
        }
        return nil
    }
}
