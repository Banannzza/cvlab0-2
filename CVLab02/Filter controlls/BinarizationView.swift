//
//  BinarizationView.swift
//  CVLab0-2
//
//  Created by Алексей Остапенко on 18/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit

protocol BinarizationViewDelegate {
    func parametersChanged(min: UInt8, max: UInt8)
}


class BinarizationView: UIView {

    var delayTimer: Timer!
    var delay = false
    var delegate: BinarizationViewDelegate?
    
    let step: Float = 1.0
    
    @IBOutlet weak var maxSlider: UISlider!
    @IBOutlet weak var minSlider: UISlider!
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let roundedValue = round(sender.value / step) * step
        sender.value = roundedValue
        if (delay) {
            self.turnOnTimer()
        }
        else {
            self.delegate?.parametersChanged(min: UInt8(self.minSlider.value), max: UInt8(self.maxSlider.value))
        }
    }
    func timerTick() {
        self.delegate?.parametersChanged(min: UInt8(self.minSlider.value), max: UInt8(self.maxSlider.value))
    }

    private func turnOnTimer() {
        if (self.delayTimer != nil) {
            self.delayTimer.invalidate()
        }
        self.delayTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timerTick), userInfo: nil, repeats: false)
    }

}
