//
//  GammaCorrectionView.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 14/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit


protocol GammaCorrectionViewDelegate {
    func parametersChanged(c: Float, y: Float)
}

class GammaCorrectionView: UIView {

   
    var delegate: GammaCorrectionViewDelegate?
    
    @IBOutlet weak var cSlider: UISlider!
    @IBOutlet weak var ySlider: UISlider!
    @IBOutlet weak var cValueLabel: UILabel!
    @IBOutlet weak var yValueLabel: UILabel!
    
    let step: Float = 0.05
    var delay = false
    var delayTimer: Timer!

    
    
    
    @IBAction func ySliderValueChanged(_ sender: AnyObject) {
        let roundedValue = round(sender.value / step) * step
        ySlider.value = roundedValue
        yValueLabel.text = String(ySlider.value)
        if (delay) {
            self.turnOnTimer()
        }
        else {
            self.delegate?.parametersChanged(c: self.cSlider.value, y: self.ySlider.value)
        }
       
    }
    
    @IBAction func cSliderValueChanged(_ sender: AnyObject) {
        let roundedValue = round(sender.value / step) * step
        cSlider.value = roundedValue
        cValueLabel.text = String(cSlider.value)
        if (delay) {
            self.turnOnTimer()
        }
    }
    
    func timerTick() {
          self.delegate?.parametersChanged(c: self.cSlider.value, y: self.ySlider.value)
    }
    
    private func turnOnTimer() {
        if (self.delayTimer != nil) {
            self.delayTimer.invalidate()
        }
        self.delayTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timerTick), userInfo: nil, repeats: false)
    }
}
