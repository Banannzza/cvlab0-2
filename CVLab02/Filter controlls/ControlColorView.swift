//
//  ControlColorView.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 14/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit

protocol ControlColorViewDelegage {
    func setDestenationColor(color: Color)
}


class ControlColorView: UIView {

    @IBOutlet weak var blueSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var redSlider: UISlider!

    @IBOutlet weak var srcView: UIView!
    @IBOutlet weak var dstView: UIView!
    
    let step: Float = 1
    var delayTimer: Timer!
    var delay = false
    var delegate: ControlColorViewDelegage?
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let roundedValue = round(sender.value / step) * step
        sender.value = roundedValue
        self.dstView.backgroundColor = UIColor(red: CGFloat(self.redSlider.value) / 255, green: CGFloat(self.greenSlider.value) / 255, blue: CGFloat(self.blueSlider.value) / 255, alpha: 1)
        if (delay) {
            self.turnOnTimer()
        }
        else {
            self.delegate?.setDestenationColor(color: Color(B: UInt8(self.blueSlider.value), G: UInt8(self.greenSlider.value), R: UInt8(self.redSlider.value)))
        }
    }
    func timerTick() {
         self.delegate?.setDestenationColor(color: Color(B: UInt8(self.blueSlider.value), G: UInt8(self.greenSlider.value), R: UInt8(self.redSlider.value)))
    }
    
    private func turnOnTimer() {
        if (self.delayTimer != nil) {
            self.delayTimer.invalidate()
        }
        self.delayTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timerTick), userInfo: nil, repeats: false)
    }
}

extension ControlColorView: ControlColorFilterDelegate {
    func getPixelColor(color: Color) {
        DispatchQueue.main.async {
            self.srcView.backgroundColor = UIColor(red: CGFloat(color.R) / 255, green: CGFloat(color.G) / 255, blue: CGFloat(color.B) / 255, alpha: CGFloat(color.A) / 255)
            self.redSlider.value = Float(color.R)
            self.blueSlider.value = Float(color.B)
            self.greenSlider.value = Float(color.G)
            self.dstView.backgroundColor = UIColor(red: CGFloat(self.redSlider.value) / 255, green: CGFloat(self.greenSlider.value) / 255, blue: CGFloat(self.blueSlider.value) / 255, alpha: CGFloat(color.A) / 255)
        }
    }
}
