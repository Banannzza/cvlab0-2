//
//  CameraModel.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 12/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import Accelerate




class CameraModel: NSObject {

    var captureSession: AVCaptureSession
    private var captureDevice: AVCaptureDevice
    private var deviceInput: AVCaptureDeviceInput?
    private var dataOutput: AVCaptureVideoDataOutput

    
    var userFilter: Filter?
    var previewLayer: CALayer?
    var connection: AVCaptureConnection?
    
    
    init(sessionPreset:String) {
        self.captureSession = AVCaptureSession()
        self.captureSession.sessionPreset = sessionPreset
        self.captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        do {
            try self.deviceInput = AVCaptureDeviceInput(device: captureDevice)
            if self.captureSession.canAddInput(self.deviceInput) {
                captureSession.addInput(self.deviceInput)
            }
            else {
                print("CaptureSession cant add deviceInput!")
            }
        }
        catch _ {
            print("AVCaptureDeviceInput init error!")
        }
        self.dataOutput = AVCaptureVideoDataOutput()
        self.dataOutput.videoSettings = [String(kCVPixelBufferPixelFormatTypeKey) : Int(kCVPixelFormatType_32BGRA)]
        self.dataOutput.alwaysDiscardsLateVideoFrames = true
        
        if captureSession.canAddOutput(self.dataOutput) {
            captureSession.addOutput(self.dataOutput)
        }
        else {
            print("CaptureSession cant add dataOutput!")
        }
        
        self.connection = dataOutput.connection(withMediaType: AVMediaTypeVideo)
        self.connection?.videoOrientation = .portrait
        
    }
    
    func setNew(sessionPreset: String) {
        self.captureSession.stopRunning()
        self.captureSession.sessionPreset = sessionPreset
        self.captureSession.startRunning()
    }
    
    func makePreviewLayerFor(view: UIView) {
        self.previewLayer?.removeFromSuperlayer()
        self.previewLayer = CALayer()
        if let preview = self.previewLayer {
            //let frm = CGRect(x: 0, y: 0, width: 375, height: 618)
            preview.frame = view.frame;
           // view.layer.co
           // preview.position = CGPoint(x: frm.size.width / 2, y: frm.size.height / 2)
           // preview.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(M_PI / 2)))
            view.layer.addSublayer(preview)
        }
    }
    
    func createQueue() {
        let queue = DispatchQueue(label: "VideoQueue")
        self.dataOutput.setSampleBufferDelegate(self, queue: queue)
    }
    
    func startSession() {
        self.createQueue()
        self.captureSession.startRunning()
    }
}


extension CameraModel: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
            NotificationCenter.default.post(name: Notification.Name("CameraModelSampleBuffer"), object: sampleBuffer)
    }
}




