//
//  PreviewPhotoCollectionViewController.swift
//  CVLab0-2
//
//  Created by Алексей Остапенко on 15/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit

private let reuseIdentifier = "PreviewPhotoCell"


class PreviewPhotoCollectionViewController: UICollectionViewController {
    
    var cameraModel: CameraModel?
    var filters = [Filter]()
    var selectedItem: IndexPath!
    var image: CGImage?
    let sectionInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFilters()
        self.collectionView?.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addFilters() {
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue).union(.byteOrder32Big)
        
        let widthPerItem = self.getCellWidth()
        self.filters.append(GrayWorldFilter(bitmapInfo: bitmapInfo))
        self.filters.append(HalfToneFilter(bitmapInfo: bitmapInfo))
        self.filters.append(GammaFilter(bitmapInfo: bitmapInfo))
        self.filters.append(ControlColorFilter(bitmapInfo: bitmapInfo))
        self.filters.append(Binarization(bitmapInfo: bitmapInfo))
        self.filters.append(HistogramFilter(bitmapInfo: bitmapInfo, channel: .Red, forWidth: widthPerItem, forHeight: widthPerItem))
        self.filters.append(HistogramFilter(bitmapInfo: bitmapInfo, channel: .Green, forWidth: widthPerItem, forHeight: widthPerItem))
        self.filters.append(HistogramFilter(bitmapInfo: bitmapInfo, channel: .Blue, forWidth: widthPerItem, forHeight: widthPerItem))
    }
    
    
    func getCellWidth() -> CGFloat {
        let paddingSpace = sectionInsets.left * (2)
        let availableWidth = view.frame.width - paddingSpace * 2
        let widthPerItem = availableWidth / 2
        return widthPerItem
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filters.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PreviewPhotoCollectionViewCell
        //self.cameraModel.addPreview(preview: cell)
        cell.setup(filter: self.filters[indexPath.row])
        cell.set(image: self.image!)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        self.selectedItem = indexPath
        let newImage = (collectionView.cellForItem(at: indexPath) as! PreviewPhotoCollectionViewCell).getImage()
        if (newImage != nil) {
            self.performSegue(withIdentifier: "showMainPhotoPreview", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVC = segue.destination as! MainPhotoPreviewViewController
        destVC.filter = self.filters[self.selectedItem!.row]
        let newImage = (self.collectionView!.cellForItem(at: self.selectedItem!) as! PreviewPhotoCollectionViewCell).getImage()
        destVC.originalImage = self.image!
        destVC.image = newImage!
    }
}

extension PreviewPhotoCollectionViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthPerItem = self.getCellWidth()
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

