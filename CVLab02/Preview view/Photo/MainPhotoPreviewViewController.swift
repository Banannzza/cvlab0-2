//
//  MainPhotoPreviewViewController.swift
//  CVLab0-2
//
//  Created by Алексей Остапенко on 15/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit

class MainPhotoPreviewViewController: UIViewController, UIScrollViewDelegate  {
    
    @IBOutlet var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
            scrollView.minimumZoomScale = 1.0
            scrollView.maximumZoomScale = 15.0
        }
    }
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
    var filter: Filter!
    var originalImage: CGImage!
    var image: CGImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        if (self.filter is HistogramFilter) {
            self.setupForHistogram()
        }
        
        if (self.filter is GammaFilter) {
            self.imageView.image = UIImage(cgImage: self.originalImage)
            self.setupForGamma()
        }
        if (self.filter is ControlColorFilter) {
            self.imageView.image = UIImage(cgImage: self.originalImage)
            self.setupColorControl()
        }
        if (self.filter is HalfToneFilter || self.filter is GrayWorldFilter) {
            self.imageView.image = UIImage(cgImage: self.image)
        }
        if (self.filter is Binarization) {
            self.imageView.image = UIImage(cgImage: self.originalImage)
            self.setupForBinarization()
        }
        
//        else {
//            self.imageView.image = UIImage(cgImage: self.image)
//        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func setupForGamma() {
        let gammaControllView = Bundle.main.loadNibNamed("GammaCorrectionView", owner: nil, options: nil)![0] as! GammaCorrectionView
        gammaControllView.frame = CGRect(x: 0, y: self.view.frame.size.height - (self.view.frame.size.height * 0.25), width: self.view.frame.size.width, height: self.view.frame.size.height * 0.25)
        self.view.addSubview(gammaControllView)
        gammaControllView.delegate = self
        self.scrollViewBottom.constant = self.view.frame.size.height * 0.25
        gammaControllView.delay = true
    }
    
    func setupColorControl() {
        let controlColorView = Bundle.main.loadNibNamed("ControlColorView", owner: nil, options: nil)![0] as! ControlColorView
        controlColorView.frame = CGRect(x: 0, y: self.view.frame.size.height - 150, width: self.view.frame.size.width, height: 150)
        self.view.addSubview(controlColorView)
        controlColorView.delegate = self
        controlColorView.delay = true
        self.scrollViewBottom.constant = 150
        
        (self.filter as! ControlColorFilter).delegate = controlColorView
        
        let tapRecongnizer = UITapGestureRecognizer(target: self, action: #selector(previewTaped))
        tapRecongnizer.numberOfTapsRequired = 1
        self.imageView.isUserInteractionEnabled = true
        self.imageView.addGestureRecognizer(tapRecongnizer)
    }
    
    func setupForBinarization() {
        let binarizationView = Bundle.main.loadNibNamed("BinarizationView", owner: nil, options: nil)![0] as! BinarizationView
        binarizationView.frame = CGRect(x: 0, y: self.view.frame.size.height - (self.view.frame.size.height * 0.25), width: self.view.frame.size.width, height: self.view.frame.size.height * 0.25)
        self.view.addSubview(binarizationView)
        binarizationView.delegate = self
        self.scrollViewBottom.constant = self.view.frame.size.height * 0.25
        binarizationView.delay = true
    }
    
    
    
    func setupForHistogram() {
        DispatchQueue.global().async {
            let hFilter = (self.filter as! HistogramFilter)
            hFilter.setSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
            let path = hFilter.getBezier()!
            DispatchQueue.main.async {
                let pathLayer: CAShapeLayer = CAShapeLayer()
                pathLayer.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
                self.imageView.layer.addSublayer(pathLayer)
                pathLayer.frame = self.view.bounds
                pathLayer.path = path.cgPath
                pathLayer.strokeColor = hFilter.channel! == .Red ? UIColor.red.cgColor : hFilter.channel! == .Blue ?  UIColor.blue.cgColor : UIColor.green.cgColor
                pathLayer.fillColor = nil
                pathLayer.lineWidth = self.view.frame.size.width / 256
                pathLayer.lineJoin = kCALineJoinBevel
                self.view.layer.addSublayer(pathLayer)

                let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
                pathAnimation.duration = 0.5
                pathAnimation.fromValue = 0.0
                pathAnimation.toValue = 1.0
                pathLayer.add(pathAnimation, forKey: "animate stroke end animation")
            }
        }
    }
    
    func previewTaped(recognizer: UIGestureRecognizer) {
        let point = recognizer.location(in: self.scrollView)
        (self.filter as! ControlColorFilter).setPixel(point: point, scaleSize: self.imageView.frame.size)
        DispatchQueue.global(qos: .utility).async {
            let _ = self.filter.applyTo(sampleImage: self.originalImage)
        }
    }
}

extension MainPhotoPreviewViewController : GammaCorrectionViewDelegate {
    func parametersChanged(c: Float, y: Float) {
            (self.filter as! GammaFilter).setParameters(y: y, c: c)
            DispatchQueue.global(qos: .utility).async {
                let image = self.filter.applyTo(sampleImage: self.originalImage)
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(cgImage: image!)
                }
            }
    }
}

extension MainPhotoPreviewViewController : ControlColorViewDelegage {
    func setDestenationColor(color: Color) {
        (self.filter as! ControlColorFilter).setControlCollor(color: color)
        DispatchQueue.global(qos: .utility).async {
            let image = self.filter.applyTo(sampleImage: self.originalImage)
            DispatchQueue.main.async {
                self.imageView.image = UIImage(cgImage: image!)
            }
        }

    }
}

extension MainPhotoPreviewViewController : BinarizationViewDelegate {
    func parametersChanged(min: UInt8, max: UInt8) {
        (self.filter as! Binarization).setParameters(min: min, max: max)
        DispatchQueue.global(qos: .utility).async {
            let image = self.filter.applyTo(sampleImage: self.originalImage)
            DispatchQueue.main.async {
                self.imageView.image = UIImage(cgImage: image!)
            }
        }
    }
}

