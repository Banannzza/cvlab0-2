//
//  PreviewPhotoCollectionViewCell.swift
//  CVLab0-2
//
//  Created by Алексей Остапенко on 15/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit

class PreviewPhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    var cellFilter: Filter?
    
    func setup(filter: Filter) {
        self.cellFilter = filter
    }
    
    func set(image: CGImage) {
        DispatchQueue.global(qos: .utility).async {
            let image = self.cellFilter?.applyTo(sampleImage: image)
            DispatchQueue.main.async {
                self.imageView.isHidden = false
                self.imageView.image = UIImage(cgImage: image!)
                let degree: Double = 90.0
                let rotationAngle = CGFloat(degree * M_PI / 180)
                let rotationTransform = CATransform3DMakeRotation(rotationAngle, 1, 0, 0)
                self.layer.transform = rotationTransform
                UIView.animate(withDuration: 0.5, animations: {
                    self.layer.transform = CATransform3DIdentity
                })
                // self.layer.contents = image
            }
        }
    }
    
    func getImage() -> CGImage? {
        return self.imageView.image?.cgImage
    }

}
