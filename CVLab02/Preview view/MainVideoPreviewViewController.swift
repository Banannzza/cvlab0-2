//
//  MainPreviewViewController.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 14/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import AVFoundation
import GLKit
import OpenGLES

class MainVideoPreviewViewController: UIViewController {

    @IBOutlet weak var previewBottom: NSLayoutConstraint!
    @IBOutlet weak var preview: UIView!

    var filter: Filter!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.setupControllView()
         NotificationCenter.default.addObserver(self, selector: #selector(self.recieveBuffer), name: Notification.Name("CameraModelSampleBuffer"), object: nil)
        
    }

    override func viewDidDisappear(_ animated: Bool) {
         NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func recieveBuffer(notification: Notification) {
        DispatchQueue.global(qos: .utility).sync {
            let image = self.filter.applyTo(sampleBuffer: notification.object as! CMSampleBuffer)
            DispatchQueue.main.sync {
                self.preview.layer.contents = image
            }
        }
    }
    
    
    func setupControllView() {
        if self.filter is GammaFilter {
            let gammaControllView = Bundle.main.loadNibNamed("GammaCorrectionView", owner: nil, options: nil)![0] as! GammaCorrectionView
            gammaControllView.frame = CGRect(x: 0, y: self.view.frame.size.height - (self.view.frame.size.height * 0.25), width: self.view.frame.size.width, height: self.view.frame.size.height * 0.25)
            self.view.addSubview(gammaControllView)
            gammaControllView.delegate = self
            self.previewBottom.constant = self.view.frame.size.height * 0.25
            return
        }
        
        if self.filter is ControlColorFilter {
            let controlColorView = Bundle.main.loadNibNamed("ControlColorView", owner: nil, options: nil)![0] as! ControlColorView
            controlColorView.frame = CGRect(x: 0, y: self.view.frame.size.height - 150, width: self.view.frame.size.width, height: 150)
            self.view.addSubview(controlColorView)
            controlColorView.delegate = self
            self.previewBottom.constant = 150
            
            (self.filter as! ControlColorFilter).delegate = controlColorView
            
            let tapRecongnizer = UITapGestureRecognizer(target: self, action: #selector(previewTaped))
            tapRecongnizer.numberOfTapsRequired = 1

            self.preview.addGestureRecognizer(tapRecongnizer)
            return
        }
        
        if self.filter is Binarization {
            let binarizationView = Bundle.main.loadNibNamed("BinarizationView", owner: nil, options: nil)![0] as! BinarizationView
            binarizationView.frame = CGRect(x: 0, y: self.view.frame.size.height - (self.view.frame.size.height * 0.25), width: self.view.frame.size.width, height: self.view.frame.size.height * 0.25)
            self.view.addSubview(binarizationView)
            binarizationView.delegate = self
            self.previewBottom.constant = self.view.frame.size.height * 0.25
        }
        
        if self.filter is HistogramFilter {
            (self.filter as! HistogramFilter).setSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        self.previewBottom.constant = 0
        
    }

    func previewTaped(recognizer: UIGestureRecognizer) {
        let point = recognizer.location(in: self.preview)
        print(point)
        (self.filter as! ControlColorFilter).setPixel(point: point, scaleSize: self.preview.frame.size)
    }
}


extension MainVideoPreviewViewController : GammaCorrectionViewDelegate {
    func parametersChanged(c: Float, y: Float) {
        (self.filter as! GammaFilter).setParameters(y: y, c: c)
    }
}

extension MainVideoPreviewViewController : ControlColorViewDelegage {
    func setDestenationColor(color: Color) {
        (self.filter as! ControlColorFilter).setControlCollor(color: color)
    }
}

extension MainVideoPreviewViewController : BinarizationViewDelegate {
    func parametersChanged(min: UInt8, max: UInt8) {
        (self.filter as! Binarization).setParameters(min: min, max: max)
    }
}

