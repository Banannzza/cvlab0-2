//
//  PreviewCollectionViewController.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 13/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import AVFoundation

private let reuseIdentifier = "PreviewVideoCell"


class PreviewVideoCollectionViewController: UICollectionViewController {

    var cameraModel: CameraModel?
    var filters = [Filter]()
    var selectedItem: IndexPath!
    var enabled = true
    let sectionInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.addFilters()
        self.collectionView?.reloadData()
        self.cameraModel = CameraModel(sessionPreset: AVCaptureSessionPresetLow)
        self.cameraModel?.startSession()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !enabled {
            DispatchQueue.global(qos: .utility).sync {
                if (self.filters[self.selectedItem.row] is HistogramFilter) {
                    let widthPerItem = self.getCellWidth()
                    (self.filters[self.selectedItem.row] as! HistogramFilter).setSize(width: widthPerItem, height: widthPerItem)
                }
                if (self.filters[self.selectedItem.row] is GammaFilter) {
                    (self.filters[self.selectedItem.row] as! GammaFilter).setParameters(y: 1, c: 1)
                }
                if (self.filters[self.selectedItem.row] is ControlColorFilter) {
                    (self.filters[self.selectedItem.row] as! ControlColorFilter).clearSelectedColors()
                }
                self.cameraModel?.setNew(sessionPreset: AVCaptureSessionPresetLow)
                self.setCells(enabled: true)
                self.enabled = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addFilters() {
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue).union(.byteOrder32Little)
        let widthPerItem = self.getCellWidth()
        self.filters.append(GrayWorldFilter(bitmapInfo: bitmapInfo))
        self.filters.append(HalfToneFilter(bitmapInfo: bitmapInfo))
        self.filters.append(GammaFilter(bitmapInfo: bitmapInfo))
        self.filters.append(ControlColorFilter(bitmapInfo: bitmapInfo))
        self.filters.append(Binarization(bitmapInfo: bitmapInfo))
        self.filters.append(HistogramFilter(bitmapInfo: bitmapInfo, channel: .Red, forWidth: widthPerItem, forHeight: widthPerItem))
        self.filters.append(HistogramFilter(bitmapInfo: bitmapInfo, channel: .Green, forWidth: widthPerItem, forHeight: widthPerItem))
        self.filters.append(HistogramFilter(bitmapInfo: bitmapInfo, channel: .Blue, forWidth: widthPerItem, forHeight: widthPerItem))
    }

    func setCells(enabled: Bool) {
        for i in 0..<self.filters.count {
            let index = IndexPath(row: i, section: 0)
            (self.collectionView?.cellForItem(at: index) as! PreviewVideoCollectionViewCell).set(enabled: enabled)
        }
    }
    
    func getCellWidth() -> CGFloat {
        let paddingSpace = sectionInsets.left * (2)
        let availableWidth = view.frame.width - paddingSpace * 2
        let widthPerItem = availableWidth / 2
        return widthPerItem
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filters.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PreviewVideoCollectionViewCell
        cell.setup(filter: self.filters[indexPath.row])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedItem = indexPath
        self.enabled = false
        self.setCells(enabled: false)
        self.performSegue(withIdentifier: "showMainVideoPreview", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (self.filters[self.selectedItem.row] is GammaFilter) {
            self.cameraModel?.setNew(sessionPreset:AVCaptureSessionPresetLow)
        }
        else
            if (self.filters[self.selectedItem.row] is HistogramFilter) {
                 self.cameraModel?.setNew(sessionPreset:AVCaptureSessionPreset640x480)
            }
            else {
                self.cameraModel?.setNew(sessionPreset:AVCaptureSessionPreset640x480)
            }
        let destVC = segue.destination as! MainVideoPreviewViewController
        destVC.filter = self.filters[self.selectedItem.row]
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell:
        UICollectionViewCell, forItemAt indexPath: IndexPath) {

        let degree: Double = 90.0
        let rotationAngle = CGFloat(degree * M_PI / 180)
        let rotationTransform = CATransform3DMakeRotation(rotationAngle, 1, 0, 0)
        cell.layer.transform = rotationTransform
        UIView.animate(withDuration: 0.5, animations: {
            cell.layer.transform = CATransform3DIdentity
        })
    }
}

extension PreviewVideoCollectionViewController : UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthPerItem = self.getCellWidth()
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
