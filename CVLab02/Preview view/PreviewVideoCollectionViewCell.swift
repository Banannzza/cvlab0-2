//
//  PreviewCollectionViewCell.swift
//  CVLab02
//
//  Created by Алексей Остапенко on 13/03/17.
//  Copyright © 2017 Алексей Остапенко. All rights reserved.
//

import UIKit
import AVFoundation

class PreviewVideoCollectionViewCell: UICollectionViewCell {
    
    
    var cellFilter: Filter?
    
    func setup(filter: Filter) {
        self.cellFilter = filter
        self.set(enabled: true)
    }
    
    func recieveBuffer(notification: Notification) {
         DispatchQueue.global(qos: .utility).sync {
            let image = self.cellFilter?.applyTo(sampleBuffer: notification.object as! CMSampleBuffer)
            DispatchQueue.main.async {
                self.layer.contents = image
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        NotificationCenter.default.removeObserver(self)
    }
    
    func set(enabled: Bool) {
        if (enabled) {
             NotificationCenter.default.addObserver(self, selector: #selector(self.recieveBuffer), name: Notification.Name("CameraModelSampleBuffer"), object: nil)
        }
        else {
            NotificationCenter.default.removeObserver(self)
        }
    }
    
}
